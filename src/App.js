import { Button, Row, Col, Input, Menu, Breadcrumb } from "antd";

import "antd/dist/antd.css";
import "./App.css";
import Heading from "./components/Heading";
import CalcBox from "./components/CalcBox";

function App() {
  return (
    <>
      {/* Navbar */}
      <Row className="navbar-container" justify="space-around" align="middle">
        <Col span={8}>
          <span className="logo">Logo</span>
        </Col>
        <Col span={12}>
          <Menu className="navbar" mode="horizontal" defaultSelectedKeys="/">
            <Menu.Item>
              <a href="https://www.calculator.net">Financial</a>
            </Menu.Item>
            <Menu.Item>
              <a href="https://www.calculator.net">Fitness & Health</a>
            </Menu.Item>
            <Menu.Item>
              <a href="https://www.calculator.net">Math</a>
            </Menu.Item>
            <Menu.Item>
              <a href="https://www.calculator.net">Others</a>
            </Menu.Item>
          </Menu>
        </Col>
      </Row>

      {/* Breadcrumb */}
      <Row justify="center">
        <Col span={22}>
          <Breadcrumb className="mt-8" style={{ fontSize: "12px" }}>
            <Breadcrumb.Item>
              <a href="https://www.calculator.net">Home</a>
            </Breadcrumb.Item>
            <Breadcrumb.Item>
              <a href="https://www.calculator.net/financial">Financial</a>
            </Breadcrumb.Item>
            <Breadcrumb.Item>
              <a href="https://www.calculator.net">
                Rental Property Calculator
              </a>
            </Breadcrumb.Item>
          </Breadcrumb>
        </Col>
      </Row>

      {/* Main Content Area */}
      <Row justify="center">
        <Col span={16}>
          <h1 className="ptb-5">Rental Property Calculator</h1>
          <div className="text-center">
            <Heading text="Modify values and click the Calculate button to use" />
          </div>

          {/* Calculation Area */}
          <CalcBox />

          {/* Related Links */}
          <Row>
            <Col span={24} className="related-links">
              <span className="rl-label">Related</span>
              <a className="rl-links" href="https://www.calculator.net">
                Investment Calculator
              </a>
              <a className="rl-links" href="https://www.calculator.net">
                Average Return Calculator
              </a>
              <a className="rl-links" href="https://www.calculator.net">
                Mortgage Calculator
              </a>
            </Col>
          </Row>
          <div>
            <h2 className="ptb-5">Rental Property Investments</h2>
            <p>
              Rental property investment refers to real estate investment that
              involves real estate and its purchase, followed by the holding,
              leasing, and selling of it. Depending on the type of rental
              property, investors need a certain level of expertise and
              knowledge to profit from their ventures. Real property can be most
              properties that are leasable, such as a single unit, a duplex, a
              single-family home, an entire apartment complex, a commercial
              retail plaza, or an office space. In some cases, industrial
              properties can also be used as rental property investments. More
              commercial rental properties, such as apartment complexes or
              office buildings, are more complicated and difficult to analyze
              due to a variety of factors that result from the larger scale. For
              older properties, it is typical to assume higher maintenance and
              repair costs. Rental property investments are generally
              capital-intensive and cash flow dependent with low levels of
              liquidity. However, compared with equity markets, rental property
              investments are normally more stable, have tax benefits, and are
              more likely to hedge against inflation. Given proper financial
              analysis, they can turn out to be profitable and worthwhile
              investments. The Rental Property Calculator can help run the
              numbers.
            </p>
            <h3 className="ptb-5">Income</h3>
            <p>
              There are several ways in which rental property investments earn
              income. The first is that investors earn regular cash flow,
              usually on a monthly basis, in the form of rental payment from
              tenants. In addition, as with the ownership of any equity, rental
              properties give the investor the possibility of earning profit
              from the appreciation, or increase in value over time, of the
              property. Unlike rental income, a sale provides one large, single
              return.
            </p>
            <h3 className="ptb-5">Responsibilities</h3>
            <p>
              Rental property investing is not passive income. It requires time
              and work. The investor or owner has to take on the role of the
              landlord and all the job responsibilities associated with it.
            </p>
            General responsibilities of owning a rental property include:
            <ul>
              <ol>
                Tenant Management—finding tenants, performing background
                screenings for potential tenants, creating legal lease
                contracts, collecting rent, and evicting tenants if necessary.
              </ol>
              <ol>Property Maintenance—repairs, upkeep, renovations, etc.</ol>
              <ol>
                Administrative—filing paperwork, setting rent, handling taxes,
                paying employees, budgeting, etc.
              </ol>
            </ul>
            It is common for rental property owners to hire property management
            companies at a fixed or percentage fee to handle all the
            responsibilities. Investors who have limited time, who don't live
            near their rental property, who aren't interested in hands-on
            management, or who can afford the cost can benefit from hiring a
            property management company. This is roughly estimated to cost about
            10% of rental property income.
            <h2 className="ptb-5">General Guidelines</h2>
            <p>
              Real estate investing can be complex, but there are some general
              principles that are useful as quick starting points when analyzing
              investments. However, every market is different, and it is very
              possible that these guidelines will not work for certain
              situations. It is important that they be treated as such, not as
              replacements for hard financial analysis nor advice from real
              estate professionals.
              <em>50% Rule</em>—A rental property's sum of operating expenses
              hover around 50% of income. Operating expenses do not include
              mortgage principal or interest. The other 50% can be used to pay
              the monthly mortgage payment. This can be used to quickly estimate
              the cash flow and profit of an investment.
              <em>1% Rule</em>—The gross monthly rent income should be 1% or
              more of the property purchase price, after repairs. It is not
              uncommon to hear of people who use the 2% or even 3% Rule – the
              higher the better. A lesser known rule is the 70% Rule. This is a
              rule for purchasing and flipping distressed real estate for a
              profit, which states that purchase price should be less than 70%
              of after-repair value (ARV) minus repair costs (rehab).
            </p>
            <h2 className="ptb-5">Internal Rate of Return</h2>
            <p>
              Internal rate of return (IRR) or annualized total return is an
              annual rate earned on each dollar invested for the period it is
              invested. It is generally used by most if not all investors as a
              way to compare different investments. The higher the IRR, the more
              desirable the investment. IRR is one of, if not the most important
              measure of the profitability of a rental property; capitalization
              rate is too basic, and Cash Flow Return on Investment (CFROI) does
              not account for the time value of money.
            </p>
            <h2 className="ptb-5">Capitalization Rate</h2>
            <p>
              Capitalization rate, often called the cap rate, is the ratio of
              net operating income (NOI) to the investment asset value or
              current market value. Cap rate = Net Operating Income Price Cap
              rate is the best indicator for quick investment property
              comparisons. It can also be useful to evaluate the past cap rates
              of a property to gain some insight into how the property has
              performed in the past, which may allow the investor to extrapolate
              how the property may perform in the future.
            </p>
            If it is particularly complex to measure net operating income for a
            given rental property, discounted cash flow analysis can be a more
            accurate alternative.
            <h2 className="ptb-5">Cash Flow Return on Investment</h2>
            <p>
              When purchasing rental properties with loans, cash flows need to
              be examined carefully. Rental property investment failures can be
              caused by unsustainable, negative cash flows. Cash Flow Return on
              Investment (CFROI) is a metric for this. Sometimes called
              Cash-on-Cash Return, CFROI helps investors identify the
              losses/gains associated with ongoing cash flows. Sustainable
              rental properties should generally have increasing annual CFROI
              percentages, usually due to static mortgage payments along with
              rent incomes that appreciate over time. Things to Keep in Mind
              Generally, the higher an investment's IRR, CFROI, and cap rate,
              the better. In the real world, it is very unlikely that an
              investment in a rental property goes exactly as planned or as
              calculated by this Rental Property Calculator. Making so many
              financial assumptions extended over long periods of time (usually
              several decades) may result in undesirable/unexpected surprises.
              Whether a short recession depreciates the value of a property
              significantly, or construction of a thriving shopping complex
              inflates values, both can have drastic influences on cap rate,
              IRR, and CFROI. Even mid-level changes such as hikes in
              maintenance costs or vacancy rates can affect the numbers. Monthly
              rent may also fluctuate drastically from year to year, so taking
              the estimated rent from a certain time and extrapolating it
              several decades into the future based on an appreciation rate
              might not be realistic. Furthermore, while appreciation of values
              is accounted for, inflation is not, which might distort such large
              figures drastically.
            </p>
            <h2 className="ptb-5">Other Types of Real Estate Investments</h2>
            <p>
              Aside from rental properties, there are many other ways to invest
              in real estate. The following lists a few other common
              investments.
            </p>
            <h2 className="ptb-5">REITs</h2>
            <p>
              Real Estate Investment Trusts (REITs) are companies that let
              investors pool their money to make debt or equity investments in a
              collection of properties or other real estate assets. REITs can be
              classified as private, publicly-traded, or public non-traded.
              REITs are ideal for investors who want portfolio exposure to real
              estate without having to go through a traditional real estate
              transaction. For the most part, REITs are a source of passive
              income as part of a diversified portfolio of investments that
              generally includes stocks and bonds.
            </p>
            <h2 className="ptb-5">Buy and Sell</h2>
            <p>
              Buying and selling (sometimes called real estate trading) is
              similar to rental property investing, except there is no or little
              leasing out involved. Generally, real estate is purchased,
              improvements are made, and it is then sold for profit, usually in
              a short time frame. Sometimes no improvements are made. When
              buying and selling houses, it is commonly called house flipping.
              Buying and selling real estate for profit generally requires deep
              market knowledge and expertise.
            </p>
            <h2 className="ptb-5">Wholesaling</h2>
            <p className="mb-8">
              Wholesaling is the process of finding real estate deals, writing a
              contract to acquire the deal, then selling the contract to another
              buyer. The wholesaler never actually owns the real estate.
            </p>
          </div>
        </Col>
        <Col className="p-8 mt-8" span={6}>
          {/* Search */}
          <Row align="center">
            <Col className="mr-5">
              <Input />
            </Col>
            <Col>
              <Button className="search-btn">Search</Button>
            </Col>
          </Row>

          {/* Extra Links */}
          <div className="mtb-8 p-8">
            <Heading text="Financial Calculator" />
            <Row justify="space-between" className="calculator-links">
              <Col span={8}>
                <a className="link" href="https://www.calculator.net">
                  Mortgage
                </a>
                <a className="link" href="https://www.calculator.net">
                  Auto Loan
                </a>
                <a className="link" href="https://www.calculator.net">
                  Payment
                </a>
                <a className="link" href="https://www.calculator.net">
                  Currency
                </a>
                <a className="link" href="https://www.calculator.net">
                  Income Tax
                </a>
                <a className="link" href="https://www.calculator.net">
                  Salary
                </a>
                <a className="link" href="https://www.calculator.net">
                  Interest rate
                </a>
              </Col>
              <Col span={8}>
                <a className="link" href="https://www.calculator.net">
                  Loan
                </a>
                <a className="link" href="https://www.calculator.net">
                  Retirement
                </a>
                <a className="link" href="https://www.calculator.net">
                  Investment
                </a>
                <a className="link" href="https://www.calculator.net">
                  inflation
                </a>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>

      {/* Footer */}
      <Row justify="center" className="footer">
        <a className="link" href="https://www.calculator.net/about">
          About us
        </a>
        <a className="link" href="https://www.calculator.net/about">
          Site map
        </a>
        <a className="link" href="https://www.calculator.net/about">
          Terms of use
        </a>
        <a className="link" href="https://www.calculator.net/about">
          Privacy policy
        </a>
      </Row>
    </>
  );
}

export default App;
