function Heading({ text }) {
  return (
    <div className='heading'>
      <span className='title'>{text}</span>
    </div>
  )
}

export default Heading;
