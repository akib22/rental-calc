import { Form, Row, Col, Button, Input, Radio, InputNumber } from "antd";
import { useState } from "react";

import Heading from "./Heading";

const numberValidator = (_, value) => {
  if (!value) {
    return Promise.reject("Value is required.");
  }

  if (isNaN(parseFloat(value))) {
    return Promise.reject("Value should be a number.");
  }

  return Promise.resolve();
};

function CalcBox() {
  const [form] = Form.useForm();

  const onReset = () => {
    form.resetFields();
  };

  const [useLoan, setUseLoan] = useState(true);
  const [needRepair, setNeedRepair] = useState(false);
  const [knowSellPrice, setKnowSellPrice] = useState(false);

  const onFinish = (values) => {
    console.log("Received values of form: ", values);
    // UPDATE-ME: Calculate your data here
    onReset();
  };

  return (
    <Form form={form} onFinish={onFinish}>
      <Row className="calc-box" justify="space-between">
        <Col span={11}>
          <div className="mb-8">
            <Heading text="Purchase" />
            <div>
              <Form.Item label="Purchase Price" style={{ marginBottom: "5px" }}>
                <Form.Item
                  name="purchasePrice"
                  rules={[{ validator: numberValidator }]}
                  noStyle
                >
                  <Input prefix="$" />
                </Form.Item>{" "}
              </Form.Item>
              <Form.Item label="Use Loan?" style={{ marginBottom: "5px" }}>
                <Radio.Group value={useLoan}>
                  <Radio value={true} onChange={() => setUseLoan(true)}>
                    Yes
                  </Radio>
                  <Radio value={false} onChange={() => setUseLoan(false)}>
                    No
                  </Radio>
                </Radio.Group>
              </Form.Item>
              {useLoan && (
                <>
                  <Form.Item
                    label="Down Payment"
                    style={{ marginBottom: "5px" }}
                  >
                    <Form.Item
                      name="downPayment"
                      rules={[
                        {
                          validator: numberValidator,
                        },
                      ]}
                      noStyle
                    >
                      <Input suffix="%" />
                    </Form.Item>
                  </Form.Item>
                  <Form.Item
                    label="Interest Rate"
                    style={{ marginBottom: "5px" }}
                  >
                    <Form.Item
                      name="interestRate"
                      rules={[
                        {
                          validator: numberValidator,
                        },
                      ]}
                      noStyle
                    >
                      <Input suffix="%" />
                    </Form.Item>
                  </Form.Item>
                  <Form.Item label="Loan Term" style={{ marginBottom: "5px" }}>
                    <Form.Item
                      name="loanTerm"
                      rules={[
                        {
                          validator: numberValidator,
                        },
                      ]}
                      noStyle
                    >
                      <InputNumber min={1} />
                    </Form.Item>
                    <span className="ant-form-text"> Years</span>
                  </Form.Item>
                </>
              )}
              <Form.Item label="Closing Cost" style={{ marginBottom: "5px" }}>
                <Form.Item
                  name="closingCost"
                  rules={[
                    {
                      validator: numberValidator,
                    },
                  ]}
                  noStyle
                >
                  <Input prefix="$" />
                </Form.Item>
              </Form.Item>
              <Form.Item label="Need Repairs? " style={{ marginBottom: "5px" }}>
                <Radio.Group value={needRepair}>
                  <Radio value={true} onChange={() => setNeedRepair(true)}>
                    Yes
                  </Radio>
                  <Radio value={false} onChange={() => setNeedRepair(false)}>
                    No
                  </Radio>
                </Radio.Group>
              </Form.Item>
              {needRepair && (
                <>
                  <Form.Item
                    label="Repair Cost"
                    style={{ marginBottom: "5px" }}
                  >
                    <Form.Item
                      name="repairCost"
                      rules={[
                        {
                          validator: numberValidator,
                        },
                      ]}
                      noStyle
                    >
                      <Input prefix="$" />
                    </Form.Item>
                  </Form.Item>
                  <Form.Item
                    label="Value after Repairs"
                    style={{ marginBottom: "5px" }}
                  >
                    <Form.Item
                      name="valueAfterRepairs"
                      rules={[
                        {
                          validator: numberValidator,
                        },
                      ]}
                      noStyle
                    >
                      <Input prefix="$" />
                    </Form.Item>
                  </Form.Item>
                </>
              )}
            </div>
          </div>
          <div className="mb-8">
            <Heading text="Recurring Operating Expenses" />
            <div>
              <Row align="bottom">
                <Col span={12}></Col>
                <Col span={6}>Annual</Col>
                <Col span={6}>Annual Increase</Col>
              </Row>
              <Form.Item label="Property Tax" style={{ marginBottom: "5px" }}>
                <Form.Item
                  name="propertyTaxAnnual"
                  rules={[
                    {
                      validator: numberValidator,
                    },
                  ]}
                  style={{
                    display: "inline-block",
                    width: "calc(50% - 8px)",
                    marginBottom: 0,
                  }}
                >
                  <Input prefix="$" />
                </Form.Item>
                <Form.Item
                  name="propertyTaxAnnualIncrease"
                  rules={[
                    {
                      validator: numberValidator,
                    },
                  ]}
                  style={{
                    display: "inline-block",
                    width: "calc(50% - 8px)",
                    margin: "0 8px 0 8px",
                  }}
                >
                  <Input suffix="%" />
                </Form.Item>
              </Form.Item>
              <Form.Item
                label="Total Insurance"
                style={{ marginBottom: "5px" }}
              >
                <Form.Item
                  name="totalInsuranceAnnual"
                  rules={[
                    {
                      validator: numberValidator,
                    },
                  ]}
                  style={{
                    display: "inline-block",
                    width: "calc(50% - 8px)",
                    marginBottom: 0,
                  }}
                >
                  <Input prefix="$" />
                </Form.Item>
                <Form.Item
                  name="totalInsuranceAnnualIncrease"
                  rules={[
                    {
                      validator: numberValidator,
                    },
                  ]}
                  style={{
                    display: "inline-block",
                    width: "calc(50% - 8px)",
                    margin: "0 8px 0 8px",
                  }}
                >
                  <Input suffix="%" />
                </Form.Item>
              </Form.Item>
              <Form.Item label="HOA Fee" style={{ marginBottom: "5px" }}>
                <Form.Item
                  name="hoaFeeAnnual"
                  rules={[
                    {
                      validator: numberValidator,
                    },
                  ]}
                  style={{
                    display: "inline-block",
                    width: "calc(50% - 8px)",
                    marginBottom: 0,
                  }}
                >
                  <Input prefix="$" />
                </Form.Item>
                <Form.Item
                  name="hoaFeeAnnualIncrease"
                  rules={[
                    {
                      validator: numberValidator,
                    },
                  ]}
                  style={{
                    display: "inline-block",
                    width: "calc(50% - 8px)",
                    margin: "0 8px 0 8px",
                  }}
                >
                  <Input suffix="%" />
                </Form.Item>
              </Form.Item>
              <Form.Item label="Maintenance" style={{ marginBottom: "5px" }}>
                <Form.Item
                  name="maintenanceAnnual"
                  rules={[
                    {
                      validator: numberValidator,
                    },
                  ]}
                  style={{
                    display: "inline-block",
                    width: "calc(50% - 8px)",
                    marginBottom: 0,
                  }}
                >
                  <Input prefix="$" />
                </Form.Item>
                <Form.Item
                  name="maintenanceAnnualIncrease"
                  rules={[
                    {
                      validator: numberValidator,
                    },
                  ]}
                  style={{
                    display: "inline-block",
                    width: "calc(50% - 8px)",
                    margin: "0 8px 0 8px",
                  }}
                >
                  <Input suffix="%" />
                </Form.Item>
              </Form.Item>
              <Form.Item label="Other Costs" style={{ marginBottom: "5px" }}>
                <Form.Item
                  name="otherCostsAnnual"
                  rules={[
                    {
                      validator: numberValidator,
                    },
                  ]}
                  style={{
                    display: "inline-block",
                    width: "calc(50% - 8px)",
                    marginBottom: 0,
                  }}
                >
                  <Input prefix="$" />
                </Form.Item>
                <Form.Item
                  name="otherCostsAnnualIncrease"
                  rules={[
                    {
                      validator: numberValidator,
                    },
                  ]}
                  style={{
                    display: "inline-block",
                    width: "calc(50% - 8px)",
                    margin: "0 8px 0 8px",
                  }}
                >
                  <Input suffix="%" />
                </Form.Item>
              </Form.Item>
            </div>
          </div>
        </Col>
        <Col span={11}>
          <div className="mb-8">
            <Heading text="Income" />
            <div>
              <Row align="bottom">
                <Col span={12}></Col>
                <Col span={6}>Annual</Col>
                <Col span={6}>Annual Increase</Col>
              </Row>
              <Form.Item label="Monthly Rent" style={{ marginBottom: "5px" }}>
                <Form.Item
                  name="monthlyRentAnnual"
                  rules={[
                    {
                      validator: numberValidator,
                    },
                  ]}
                  style={{
                    display: "inline-block",
                    width: "calc(50% - 8px)",
                    marginBottom: 0,
                  }}
                >
                  <Input prefix="$" />
                </Form.Item>
                <Form.Item
                  name="monthlyRentAnnualIncrease"
                  rules={[
                    {
                      validator: numberValidator,
                    },
                  ]}
                  style={{
                    display: "inline-block",
                    width: "calc(50% - 8px)",
                    margin: "0 8px 0 8px",
                  }}
                >
                  <Input suffix="%" />
                </Form.Item>
              </Form.Item>
              <Form.Item
                label="Other Monthly Income"
                style={{ marginBottom: "5px" }}
              >
                <Form.Item
                  name="monthlyIncomeAnnual"
                  rules={[
                    {
                      validator: numberValidator,
                    },
                  ]}
                  style={{
                    display: "inline-block",
                    width: "calc(50% - 8px)",
                    marginBottom: 0,
                  }}
                >
                  <Input prefix="$" />
                </Form.Item>
                <Form.Item
                  name="monthlyIncomeAnnualIncrease"
                  rules={[
                    {
                      validator: numberValidator,
                    },
                  ]}
                  style={{
                    display: "inline-block",
                    width: "calc(50% - 8px)",
                    margin: "0 8px 0 8px",
                  }}
                >
                  <Input suffix="%" />
                </Form.Item>
              </Form.Item>
              <Form.Item label="Vacancy Rate" style={{ marginBottom: "5px" }}>
                <Form.Item
                  name="vacancyRate"
                  rules={[
                    {
                      validator: numberValidator,
                    },
                  ]}
                  noStyle
                >
                  <Input suffix="%" />
                </Form.Item>
              </Form.Item>

              <Form.Item label="Management Fee" style={{ marginBottom: "5px" }}>
                <Form.Item
                  name="managementFee"
                  rules={[
                    {
                      validator: numberValidator,
                    },
                  ]}
                  noStyle
                >
                  <Input suffix="%" />
                </Form.Item>
              </Form.Item>
            </div>
          </div>
          <div>
            <Heading text="Sell" />
            <div>
              <Form.Item
                label="Do You Know the Sell Price?"
                style={{ marginBottom: "5px" }}
              >
                <Radio.Group value={knowSellPrice}>
                  <Radio value={true} onChange={() => setKnowSellPrice(true)}>
                    Yes
                  </Radio>
                  <Radio value={false} onChange={() => setKnowSellPrice(false)}>
                    No
                  </Radio>
                </Radio.Group>
              </Form.Item>
              {knowSellPrice ? (
                <Form.Item label="Sell Price" style={{ marginBottom: "5px" }}>
                  <Form.Item
                    name="sellPrice"
                    rules={[
                      {
                        validator: numberValidator,
                      },
                    ]}
                    noStyle
                  >
                    <Input prefix="$" />
                  </Form.Item>
                </Form.Item>
              ) : (
                <Form.Item
                  label="Value Appreciation"
                  style={{ marginBottom: "5px" }}
                >
                  <Form.Item
                    name="valueAppreciation"
                    rules={[
                      {
                        validator: numberValidator,
                      },
                    ]}
                    noStyle
                  >
                    <InputNumber min={1} />
                  </Form.Item>
                  <span className="ant-form-text"> Per Years</span>
                </Form.Item>
              )}

              <Form.Item label="Holding Length" style={{ marginBottom: "5px" }}>
                <Form.Item
                  name="holdingLength"
                  rules={[
                    {
                      validator: numberValidator,
                    },
                  ]}
                  noStyle
                >
                  <InputNumber min={1} />
                </Form.Item>
                <span className="ant-form-text"> Years</span>
              </Form.Item>
              <Form.Item label="Cost to Sell" style={{ marginBottom: "5px" }}>
                <Form.Item
                  name="costToSell"
                  rules={[
                    {
                      validator: numberValidator,
                    },
                  ]}
                  style={{ display: "flex", justifyContent: "space-between" }}
                >
                  <Input suffix="%" style={{ width: "100px" }} />
                </Form.Item>
              </Form.Item>
            </div>
          </div>
          <div className="btn-section">
            <Button htmlType="submit" className="calc-btn">
              Calculate
            </Button>
            <Button htmlType="button" className="clear-btn" onClick={onReset}>
              Clear
            </Button>
          </div>
        </Col>
      </Row>
    </Form>
  );
}

export default CalcBox;
