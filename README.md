For running the project please follow the steps below.
## clone the project

``` git clone https://akib22@bitbucket.org/akib22/rental-calc.git ```

## install dependencies
``` yarn install or npm i ```
## run the project 
``` yarn start ```
